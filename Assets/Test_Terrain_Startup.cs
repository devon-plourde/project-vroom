﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_Terrain_Startup : MonoBehaviour {

    Spline spline;

    //These will be generated by the line generator. This is just a sample.
    Vector2[] points = { new Vector2(217, 217),
                            new Vector2(396, 147),
                            new Vector2(495, 230),
                            new Vector2(640, 175) };

	// Use this for initialization
	void Start () {
        spline = GetComponent<Spline>();

        StartCoroutine(OnStart());
        //spline.RemoveNode(spline.nodes[0]);
	}
	
    IEnumerator OnStart()
    {
        gameObject.SetActive(false);

        SplineNode node;
        for (int i = 0; i < points.Length; ++i)
        {
            if(i == 0)
            {
                //var p = points[i];
                node = new SplineNode(points[i], Vector3.zero);
            }
            else
            {

            }
            var p = points[i];

            
            //spline.AddNode(node);
        }

        yield return null;

        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
